﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebServiceBook.Models;

namespace WebServiceBook.Controllers
{
    public class RecordsController : Controller
    {
        // GET: Records
        public ActionResult ShowRecords(int id)
        {
            var businessLayer = new RecordsBusinessLayer();
            var viewModel = businessLayer.GetRecords(id);
            return View("Records", viewModel);
        }
    }
}