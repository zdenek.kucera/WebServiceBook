﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebServiceBook.Models;
using WebServiceBook.ViewModels;

namespace WebServiceBook.Controllers
{
    public class MainController : Controller
    {
        public ActionResult Index()
        {
            VehicleBusinessLayer businessL = new VehicleBusinessLayer();
            List<Vehicle> list = new List<Vehicle>();
            list = businessL.GetVehicles();
            List<VehicleViewModel> listViewModel = new List<VehicleViewModel>();

            foreach (Vehicle vehicle in list)
            {
                VehicleViewModel vehicleVM = new VehicleViewModel();
                vehicleVM.Name = "Jmeno: " + vehicle.Name;
                vehicleVM.Type = "Typ: " + vehicle.Type;
                vehicleVM.VehicleID = vehicle.VehicleID;

                listViewModel.Add(vehicleVM);
            }

            VehicleListViewModel clvm = new VehicleListViewModel();
            clvm.ListOfVehicles = listViewModel;

            return View("Index", clvm);
        }

        public ActionResult AddNew()
        {
            return View("CreateVehicle");
        }

        public ActionResult SaveVehicle(Vehicle vehicle)
        {
            VehicleBusinessLayer saver = new VehicleBusinessLayer();
            saver.SaveVehicle(vehicle);
            return RedirectToAction("Index");
        }
    }
}