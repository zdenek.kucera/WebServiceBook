﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebServiceBook.Models;
using WebServiceBook.ViewModels;

namespace WebServiceBook.Controllers
{
    public class DeleteController : Controller
    {
        // GET: Delete
        public ActionResult DeleteMessage(int id)
        {
            DeleteMessageViewModel viewModel = new DeleteMessageViewModel();
            DeleteBusinessLayer businessLayer = new DeleteBusinessLayer();

            viewModel = businessLayer.GetDeleteMessageValues(id);
            return View("DeleteMessage", viewModel);
        }

        public ActionResult DeleteVehicle(int id)
        {
            DeleteBusinessLayer businessLayer = new DeleteBusinessLayer();
            businessLayer.DeleteVehicle(id);
            return RedirectToAction("Index", "Main");
        }
    }
}