﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebServiceBook.Models;
using WebServiceBook.ViewModels;

namespace WebServiceBook.Controllers
{
    public class UpdateController : Controller
    {
        // GET: Update
        public ActionResult UpdateVehicle(Vehicle vehicle)
        {
            var businessLayer = new UpdateBusinessLayer();
            businessLayer.UpdateVehicle(vehicle);

            return RedirectToAction("Index", "Main");
        }

        public ActionResult UpdateVehicleForm(int id)
        {
            var businessLayer = new UpdateBusinessLayer();
            var viewModel = businessLayer.GetValuesForForm(id);

            return View("UpdateVehicle", viewModel);
        }
    }
}