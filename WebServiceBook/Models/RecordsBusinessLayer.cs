﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebServiceBook.ViewModels;

namespace WebServiceBook.Models
{
    public class RecordsBusinessLayer
    {
        public RecordsListViewModel GetRecords(int id)
        {
            var database = new DataAccessLayer.WebServiceBook();
            var viewModel = new RecordsListViewModel();
            viewModel.RecordsList = (database.Records.Where(o => o.VehicleID == id).ToList());

            return viewModel;
        }
    }
}