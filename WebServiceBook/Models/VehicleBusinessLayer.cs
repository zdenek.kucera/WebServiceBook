﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebServiceBook.DataAccessLayer;
using WebServiceBook.ViewModels;

namespace WebServiceBook.Models
{
    public class VehicleBusinessLayer
    {
        public List<Vehicle> GetVehicles()
        {
            DataAccessLayer.WebServiceBook database = new DataAccessLayer.WebServiceBook();
            return database.Vehicles.ToList();
        }

        public Vehicle SaveVehicle(Vehicle vehicle)
        {
            DataAccessLayer.WebServiceBook salesDal = new DataAccessLayer.WebServiceBook();
            salesDal.Vehicles.Add(vehicle);
            salesDal.SaveChanges();
            return vehicle;
        }

        public bool IsValidUser(UserDetails u)
        {
            if (u.UserName == "Admin" && u.Password == "Admin")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}