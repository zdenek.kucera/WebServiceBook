﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebServiceBook.Models
{
    public class Record
    {
        public int ID { get; set; }

        // foreign key is generated automatically
        public int VehicleID { get; set; }
        // this property is not visible, just tells where to look for a foreign key
        private Vehicle vehicle { get; set; }

        public string Name { get; set; }
        public string Currency { get; set; }
        public string MileageUnit { get; set; }

        public int Cost { get; set; }
        public int Mileage { get; set; }
        
    }
}