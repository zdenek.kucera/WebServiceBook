﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebServiceBook.ViewModels;
using WebServiceBook.DataAccessLayer;

namespace WebServiceBook.Models
{
    public class DeleteBusinessLayer
    {
        public DeleteMessageViewModel GetDeleteMessageValues(int id)
        {
            var database = new WebServiceBook.DataAccessLayer.WebServiceBook();
            var vehicle = (Vehicle)database.Vehicles.Where(x => x.VehicleID == id).First();

            DeleteMessageViewModel viewModel = new DeleteMessageViewModel();
            viewModel.Title = "Do you really want to delete vehicle \"" + vehicle.Name + "\" ?";
            viewModel.VehicleId = id;

            return viewModel;
        }

        public void DeleteVehicle(int id)
        {
            var database = new WebServiceBook.DataAccessLayer.WebServiceBook();
            var vehicle = (Vehicle)database.Vehicles.Where(x => x.VehicleID == id).First();
            database.Vehicles.Remove(vehicle);
            database.SaveChanges();
        }
    }
}