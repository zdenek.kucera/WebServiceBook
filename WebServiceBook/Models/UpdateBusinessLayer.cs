﻿using System.Linq;
using WebServiceBook.ViewModels;

namespace WebServiceBook.Models
{
    public class UpdateBusinessLayer
    {
        public UpdateViewModel GetValuesForForm(int id)
        {
            var database = new WebServiceBook.DataAccessLayer.WebServiceBook();
            var vehicle = (Vehicle)database.Vehicles.Where(x => x.VehicleID == id).First();

            var viewModel = new UpdateViewModel();
            viewModel.Vehicle = vehicle;
            viewModel.Title = "Updating of " + vehicle.Name;

            return viewModel;
        }

        public void UpdateVehicle(Vehicle vehicle)
        {
                var database = new WebServiceBook.DataAccessLayer.WebServiceBook();
                var oldVehicle = (Vehicle)database.Vehicles.Where(x => x.VehicleID == vehicle.VehicleID).First();

                database.Entry(oldVehicle).CurrentValues.SetValues(vehicle);
                database.SaveChanges();
        }
    }
}