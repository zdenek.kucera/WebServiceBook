﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebServiceBook.Models;

namespace WebServiceBook.DataAccessLayer
{
    public class WebServiceBook : DbContext
    {
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Record> Records { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vehicle>().ToTable("Vehicle");
            modelBuilder.Entity<Record>().ToTable("Record");
            base.OnModelCreating(modelBuilder);
        }
    }
}