﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceBook.ViewModels
{
    public class DeleteMessageViewModel
    {
        public string Title { get; set; }
        public int VehicleId { get; set; }
    }
}