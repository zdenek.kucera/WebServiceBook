﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceBook.ViewModels
{
    public class VehicleListViewModel
    {
       public List<VehicleViewModel> ListOfVehicles{ get; set; }
    }
}