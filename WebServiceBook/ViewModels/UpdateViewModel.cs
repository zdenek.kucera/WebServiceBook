﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebServiceBook.Models;

namespace WebServiceBook.ViewModels
{
    public class UpdateViewModel
    {
        public Vehicle Vehicle { get; set; }
        public string Title { get; set; }
    }
}