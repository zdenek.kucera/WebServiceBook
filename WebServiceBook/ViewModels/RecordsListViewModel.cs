﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebServiceBook.Models;

namespace WebServiceBook.ViewModels
{
    public class RecordsListViewModel
    {
        public List<Record> RecordsList { get; set; }
    }
}