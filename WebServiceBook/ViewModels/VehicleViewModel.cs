﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceBook.ViewModels
{
    public class VehicleViewModel
    {
        public int VehicleID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}