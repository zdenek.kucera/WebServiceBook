namespace WebServiceBook.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class record : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Record", "VehicleID", c => c.Int(nullable: false));
            AddColumn("dbo.Record", "Currency", c => c.String());
            AddColumn("dbo.Record", "MileageUnit", c => c.String());
            AddColumn("dbo.Record", "Cost", c => c.Int(nullable: false));
            AddColumn("dbo.Record", "Mileage", c => c.Int(nullable: false));
            CreateIndex("dbo.Record", "VehicleID");
            AddForeignKey("dbo.Record", "VehicleID", "dbo.Vehicle", "VehicleID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Record", "VehicleID", "dbo.Vehicle");
            DropIndex("dbo.Record", new[] { "VehicleID" });
            DropColumn("dbo.Record", "Mileage");
            DropColumn("dbo.Record", "Cost");
            DropColumn("dbo.Record", "MileageUnit");
            DropColumn("dbo.Record", "Currency");
            DropColumn("dbo.Record", "VehicleID");
        }
    }
}
