namespace WebServiceBook.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rename : DbMigration
    {
        public override void Up()
        {
            CreateTable(
    "dbo.Vehicle",
    c => new
    {
        VehicleID = c.Int(nullable: false, identity: true),
        Name = c.String(),
        Type = c.String(),
    })
    .PrimaryKey(t => t.VehicleID);

            CreateTable(
                "dbo.Record",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
           
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Record");
            DropTable("dbo.Vehicle");
        }
    }
}
